import discord
import json
import os
import operator
from discord.ext import commands

data_file = "data.json"
config_file = "config.json"
riddles_file = "riddles.json"

# load in data files into data dict at startup
if os.stat(data_file).st_size == 0:
    data = { }
else:
    with open(data_file, "r") as r:
        data = json.load(r)

if os.stat(riddles_file).st_size == 0:
    riddles = { }
else:
    with open(riddles_file, "r") as r:
        riddles = json.load(r)

with open(config_file, "r") as r:
    config = json.load(r)

bot = commands.Bot(command_prefix=config["prefix"])

# handling the scavenger hunt questions
async def day(message, question_number):
    role = riddles[question_number]["role"]
    channel = int(riddles[question_number]["channel"])
    answer = riddles[question_number]["solutions"]
    author = str(message.author.id)

    if message.channel.id == channel and not(author in config["ignore_list"]):
        await message.delete() # delete the message so people cant see what attempts people make

        # check if author already exists, if not make it
        if not(author in data):
            data[author] = { }

        # add attempt to dict
        attempt_count_keyname = "day" + question_number + "_attempt_count"
        if (attempt_count_keyname in data[author]):
            data[author][attempt_count_keyname] += 1
        else:
            data[author][attempt_count_keyname] = 1
        attempt_count = data[author][attempt_count_keyname]

        winner_position_keyname = "day" + question_number + "_winner_position"
        if message.content in answer and not(winner_position_keyname in data[author]): # if author guessed the right answer for the first time
            winner_count_keyname = "day" + question_number + "_winner_count"

            if (not(winner_position_keyname in data[author])):
                await message.author.add_roles(discord.utils.get(message.guild.roles, name=role)) # apply configured role for progressing
                
                # add 1 to the total winner count of this question
                if (winner_count_keyname in data):
                    data[winner_count_keyname] += 1
                else:
                    data[winner_count_keyname] = 1
                
                winner_position = data[winner_count_keyname]
                data[author][winner_position_keyname] = winner_position

                points_amount = 0
                # determine points author gets  
                if ":gift:" in answer:
                    points_amount += 5
                    attempt_count_points = 0
                    winner_position_points = 0
                else:
                    winner_position_points = 0
                    attempt_count_points = 0
                    if winner_position <= 3:
                        winner_position_points += 5 - winner_position
                    else:
                        winner_position_points += 1
                    points_amount += winner_position_points
                    if (attempt_count < 5):
                        attempt_count_points = 4
                    elif (attempt_count < 10):
                        attempt_count_points = 3
                    elif (attempt_count < 20):
                        attempt_count_points = 2
                    else:
                        attempt_count_points = 1
                    points_amount += attempt_count_points
                
                # add the points to total of author
                if ("points" in data[author]):
                    data[author]["points"] += points_amount
                else:
                    data[author]["points"] = points_amount

                embed=discord.Embed(title="**" + message.author.name + " solved the mystery of day " + str(question_number) + "!**", description=str(data[winner_count_keyname]) + " people solved this mystery in total.")
                embed.set_thumbnail(url=message.author.avatar_url)
                embed.add_field(name="They now have " + str(data[author]["points"]) + " points total!", value="> " + str(attempt_count_points) + " points for making " + str(attempt_count) + " attempts." + 
                "\n> " + str(winner_position_points) + " points for being number " + str(data[author][winner_position_keyname]) + " to solve it.", inline=True)
                embed.set_footer(text="Magic Universe Scavenger Hunt")
                await bot.get_channel(int(config["updates_channel"])).send(message.author.mention, embed=embed)

        # push new dict data to json data file
        with open(data_file, "w") as w:
            json.dump(data, w)

@bot.command()
async def stats(ctx, user: discord.Member = None):
    if user:
        author = str(user.id)
        author_name = str(user.name)
        author_avatar = str(user.avatar_url)
        author_top_role = str(user.top_role)
    else:
        author = str(ctx.author.id)
        author_name = str(ctx.author.name)
        author_avatar = str(ctx.author.avatar_url)
        author_top_role = str(ctx.author.top_role)

    embed=discord.Embed(title="Statistics of " + author_name)
    embed.set_thumbnail(url=author_avatar)
    if not("points" in data[author]):
        embed.add_field(name="Total Points", value="" + str(0), inline=True)
    else:
        embed.add_field(name="Total Points", value="" + str(data[author]["points"]), inline=True)
    embed.add_field(name="Current Day", value=author_top_role, inline=True)
    embed.set_footer(text="Magic Universe Scavenger Hunt")

    await ctx.send(embed=embed)

@bot.command()
async def leaderboard(ctx):
    highest_points = 0
    pairs = { }
    list_of_points = { }
    list_of_points_descending = { }

    for key, value in data.items():
        if not(isinstance(value, int)):
            pairs[key] = key

    i = 1
    for key, value in pairs.items():
        if i == 1:
            winning_user = int(key)
        i += 1
        if "points" in data[key]:
            list_of_points[key] = points = data[key]["points"]
    list_of_points_descending = sorted(list_of_points.items(), key=operator.itemgetter(1), reverse=True)

    j = 0
    embed=discord.Embed(title="**Leaderboard Top 10**", description="")
    embed.set_footer(text="Magic Universe Scavenger Hunt")
    for key, value in list_of_points_descending:
        if not(j > 10):
            j += 1
            embed.add_field(name= "#" + str(j) + ") " + str(await bot.fetch_user(key)), value= "Points: " + str(value), inline=False)
    await ctx.send(embed=embed)

@bot.command()
async def about(ctx):
    await ctx.send(embed=discord.Embed(title="About Scavenger Hunt Bot ", description="Made by Mid ok bye"))

# listen and check for all the questions when a message is sent
@bot.event
async def on_message(message):
    count = 0
    for key in riddles:
        count += 1
        await day(message, str(count))

    await bot.process_commands(message)

# announce bot start
@bot.event
async def on_ready():
    print('Logged in as {0.user}'.format(bot))

bot.run(config["token"]) # login token